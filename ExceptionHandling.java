import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program Pembagian Bilangan");

        boolean validInput = false;
        do {
            //edit here
            try {
                System.out.print("Masukkan angka pembilang = ");
                int bilang = scanner.nextInt(); // Scanner untuk input angka pembilang

                System.out.print("Masukkan angka penyebut = ");
                int sebut = scanner.nextInt(); // Scanner untuk input angka penyebut

                int hasil = pembagian(bilang, sebut); // memanggil method pembagian untuk melakukan proses

                System.out.println("Hasilnya adalah = " + hasil);
                validInput = true; // mengubah nilai boolean validInput menjadi true agar perulangan terhenti,

            } catch (InputMismatchException e) // menggunakan InputMismatchException untuk menampilkan error message ketika angka merupakan desimal
            { 
                System.out.println("Angka harus merupakan bilangan bulat dan tidak boleh bernilai desimal");
                scanner.nextLine(); // Scanner untuk membersihkan input yang tidak valid (Agar program)

            } catch (ArithmeticException e) // menggunakan ArithmeticException untuk menampilkan error message ketika penyebut merupakan 0
            {
                System.out.println(e.getMessage());
                
            }

        } while (!validInput);

        scanner.close();
    }

    public static int pembagian(int pembilang, int penyebut) throws Exception {
        //add exception apabila penyebut bernilai 0
        if (penyebut == 0) {
            throw new ArithmeticException("Penyebut tidak dapat bernilai 0");
        }
        return pembilang / penyebut;
    }
}
